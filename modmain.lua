PrefabFiles = {
	"chihaya",
}

Assets = {
    Asset( "IMAGE", "images/saveslot_portraits/chihaya.tex" ),
    Asset( "ATLAS", "images/saveslot_portraits/chihaya.xml" ),

    Asset( "IMAGE", "images/selectscreen_portraits/chihaya.tex" ),
    Asset( "ATLAS", "images/selectscreen_portraits/chihaya.xml" ),
	
    Asset( "IMAGE", "images/selectscreen_portraits/chihaya_silho.tex" ),
    Asset( "ATLAS", "images/selectscreen_portraits/chihaya_silho.xml" ),

    Asset( "IMAGE", "bigportraits/chihaya.tex" ),
    Asset( "ATLAS", "bigportraits/chihaya.xml" ),
	
	Asset( "IMAGE", "images/map_icons/chihaya.tex" ),
	Asset( "ATLAS", "images/map_icons/chihaya.xml" ),
	
	Asset( "IMAGE", "images/avatars/avatar_chihaya.tex" ),
    Asset( "ATLAS", "images/avatars/avatar_chihaya.xml" ),
	
	Asset( "IMAGE", "images/avatars/avatar_ghost_chihaya.tex" ),
    Asset( "ATLAS", "images/avatars/avatar_ghost_chihaya.xml" ),

}

local require = GLOBAL.require
local STRINGS = GLOBAL.STRINGS

-- The character select screen lines
STRINGS.CHARACTER_TITLES.chihaya = "The IDOLM@STER"
STRINGS.CHARACTER_NAMES.chihaya = "Chihaya Kisaragi"
STRINGS.CHARACTER_DESCRIPTIONS.chihaya = "*Her soothing voice makes enemies fall asleep\n*Can call upon her dead brother for help\n*72-55-78"
STRINGS.CHARACTER_QUOTES.chihaya = "\"Quote\""

-- Custom speech strings
STRINGS.CHARACTERS.CHIHAYA = require "speech_chihaya"

-- The character's name as appears in-game 
STRINGS.NAMES.CHIHAYA = "Chihaya"

-- The default responses of examining the character
STRINGS.CHARACTERS.GENERIC.DESCRIBE.CHIHAYA = 
{
	GENERIC = "Chihaya-chan!",
	ATTACKER = "Idols don't kill people, do they?",
	MURDERER = "Idols do kill people!",
	REVIVER = "Chihaya's so kind!",
	GHOST = "Looks like she joined her brother.",
}


AddMinimapAtlas("images/map_icons/chihaya.xml")

-- Add mod character to mod character list. Also specify a gender. Possible genders are MALE, FEMALE, ROBOT, NEUTRAL, and PLURAL.
AddModCharacter("chihaya", "FEMALE")

